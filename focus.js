// var mytimeout;
var ccpic=0;
function bigpic(focus)
{
	var gallery = document.getElementById('bignews');
	var newstexts = document.getElementById('bignewstext');
	var pictures = gallery.getElementsByTagName('img');
	for (var i = 0; i < pictures.length; i++)
	{
		if (i == focus) 
		{
			pictures[i].setAttribute('sel', 'sel');
		} else 
		{
			pictures[i].setAttribute('sel', 'none');
		}
	}
	
	var titles = newstexts.getElementsByTagName('focus_title');
	var descrs= newstexts.getElementsByTagName('p');
	for (var i = 0; i < titles.length; i++)
	{
		if (i == focus) 
		{
			titles[i].setAttribute('sel', 'sel');
			descrs[i].setAttribute('sel', 'sel');
		} else 
		{
			titles[i].setAttribute('sel', 'none');
			descrs[i].setAttribute('sel', 'none');
		}
	}
	ccpic=focus;
//	clearTimeout(mytimeout);
}

function nextbigpic() 
{
	var gallery = document.getElementById('bignews');
	var pictures = gallery.getElementsByTagName('img');

	bigpic(ccpic);
	ccpic++;
	if (ccpic>=pictures.length) ccpic=0;
	mytimeout=setTimeout(nextbigpic, 3000);
}

function scroll_tpics(dir)
{
	var gallery = document.getElementById('movable_pic_content');
	var pictures = gallery.getElementsByTagName('a');
	var first_yes=-1;
	for (var i = 0; i < pictures.length; i++)
	{
		if ( (pictures[i].getAttribute("onpage")=="yes") && (first_yes<0) ) {
			first_yes=i;
		}
		pictures[i].setAttribute('onpage','no');
	}
	var x=first_yes+dir;
	var max_index=pictures.length-7;
	if (x>max_index) x=max_index;
	if (x<0) x=0;
	for (var j=x;j<x+7;j++) {
		pictures[j].setAttribute('onpage','yes');
	}
}

function scroll_uppics(direct)
{
 	var gallery = document.getElementById('up_dawn_pic');
 	var pictures = gallery.getElementsByTagName('headline');
 	var first_yes=-1;
 	for (var i = 0; i < pictures.length; i++)
 	{
  	if ( (pictures[i].getAttribute("viewpage")=="yes") && (first_yes<0) ) 
		{
   		first_yes=i;
  		}
  	pictures[i].setAttribute('viewpage','no');
 	}
 	var x=first_yes+direct;
	var max_index=pictures.length-1;
 	if (x>max_index) x=0;
 	if (x<0) x=max_index;
 	pictures[x].setAttribute('viewpage','yes');
}

function autoRotate()
{
  scroll_uppics(1);
  setTimeout(autoRotate, 2000);
}
